package pl.glowkapracuje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.glowkapracuje.entity.Guest;

public interface GuestRepository extends JpaRepository <Guest, Long> {

    Guest getFirstByEmail (String email);


}
