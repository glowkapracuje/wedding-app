package pl.glowkapracuje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.glowkapracuje.entity.Organizer;

public interface OrganizerRepository extends JpaRepository <Organizer, Long>  {

    Organizer getFirstByEmail(String email);

}
