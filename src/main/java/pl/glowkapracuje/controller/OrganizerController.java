package pl.glowkapracuje.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.glowkapracuje.entity.Organizer;
import pl.glowkapracuje.entity.Wedding;
import pl.glowkapracuje.repository.GuestRepository;
import pl.glowkapracuje.repository.OrganizerRepository;
import pl.glowkapracuje.repository.WeddingRepository;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/organizer")
//@SessionAttributes("organizer")
public class OrganizerController {

    // Wire by constructor
    OrganizerRepository organizerRepository;
    WeddingRepository weddingRepository;
    GuestRepository guestRepository;

    @Autowired
    public OrganizerController(OrganizerRepository organizerRepository,
                               WeddingRepository weddingRepository,
                               GuestRepository guestRepository) {
        this.organizerRepository = organizerRepository;
        this.weddingRepository = weddingRepository;
        this.guestRepository = guestRepository;
    }

    ////
    // GET for adding wedding form - ADD AND UPDATE
    @GetMapping("/addWedding")
    public String addWeddingForm(Model model,
                                 @RequestParam(required = false) Long id) {
        if (id == null) {
            model.addAttribute("wedding", new Wedding());
        } else {
            model.addAttribute("wedding", weddingRepository.getFirstById(id));
        }
        return "wedding/addWeddingForm";
    }

    // POST for adding wedding form
    @PostMapping("/addWedding")
    public String postAddWeddingForm(@ModelAttribute @Valid Wedding wedding,
                                     BindingResult bindingResult,
                                     HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "wedding/addWeddingForm";
        }

        Long id = (Long) session.getAttribute("id");
        Organizer organizer = organizerRepository.getOne(id);
        wedding.setOrganizer(organizer);
        weddingRepository.save(wedding);

        return "redirect:weddingList";
    }

    ////
    // WEDDING LIST BY ORGANIZER ID
    @GetMapping("weddingList")
    public String weddingListByOrganizer(Model model,
                                         HttpSession session){
        Long id = (Long)session.getAttribute("id");
        model.addAttribute("weddingList", weddingRepository.findAllByOrganizerId(id));
        model.addAttribute("listType", "personal");
        return "wedding/weddingList";
    }

    ////
    // DELETE WEDDING BY ID
    @GetMapping("/deleteWedding")
    public String deleteWedding(@RequestParam Long id){
        weddingRepository.delete(id);
        return "redirect:weddingList";
    }

}
