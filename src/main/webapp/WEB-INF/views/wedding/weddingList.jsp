<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <%--    HEADER--%>
    <%@include file="../parts/header.jspf" %>
    <%--    HEADER--%>
    <title>Title</title>
</head>
<body>
<%----%>
<div class="container">

    <div class="classicContainer row">
        <div class="classicRectangle">
            <div>

                <c:if test="${listType == 'personal'}">
                    <h2>Your Weddings List</h2>
                </c:if>
                <c:if test="${listType == 'general'}">
                    <h2>All Weddings List</h2>
                </c:if>

            </div>
            <div class="classicClean">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Address</th>
                        <th scope="col">Date</th>
                        <%--            <th scope="col">---</th>--%>
                        <th scope="col">Organizer</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="wedding" items="${weddingList}">
                        <tr>
                            <th scope="row"><c:out value="${wedding.id}"/></th>
                            <td><c:out value="${wedding.address}"/></td>
                            <td><c:out value="${wedding.date}"/></td>
                                <%--                <td><c:out value="${wedding.place.title}"/></td>--%>
                            <td><c:out value="${wedding.organizer.organizerName}"/></td>
                            <td>
                                <button class="btn btn-primary btn-sm"
                                        onclick="javascript:document.location.href='/wedding/details?id=${wedding.id}'">
                                    DETAILS
                                </button>
                                <c:if test="${organizer.id == wedding.organizer.id}">
                                    <button class="btn btn-warning btn-sm"
                                            onclick="javascript:document.location.href='/organizer/addWedding?id=${wedding.id}'">
                                        UPDATE
                                    </button>
                                    <button class="btn btn-danger btn-sm"
                                            onclick="javascript:document.location.href='/organizer/deleteWedding?id=${wedding.id}'">
                                        REMOVE
                                    </button>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <c:if test="${not empty organizer.id}">
            <div class="classicCleanWithoutMinHeight">
                <hr style="width: 100%; border: 1px dot-dash">
                <div class="classicClean">
                    <button class="btn btn-primary"
                            onclick="javascript:document.location.href='/organizer/addWedding'">
                        ADD NEW WEDDING
                    </button>
                </div>
            </div>
        </c:if>

    </div>

</div>
<%----%>
<%--FOOTER--%>
<%@include file="../parts/footer.jspf" %>
<%--FOOTER--%>
</body>
</html>
