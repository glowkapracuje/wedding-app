<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--    HEADER--%>
    <%@include file="../parts/header.jspf" %>
    <%--    HEADER--%>
    <title>Title</title>
</head>
<body>
<%----%>
<div class="container">


    <div class="classicContainer row">
        <div class="classicHalf helloImg2">

        </div>

        <div class="classicHalf">
            <div class="form-group" style="margin-left: 50px;">

                <div class="form-row">
                    <h2>Add New Wedding</h2>
                </div>

                <form:form method="post" modelAttribute="wedding">

                    <%--                <div class="form-row">--%>
                    <%--                    <div><form:errors path="guestName" cssClass="error"/></div>--%>
                    <%--                    <div><form:errors path="email" cssClass="error"/></div>--%>
                    <%--                    <div><form:errors path="password" cssClass="error"/></div>--%>
                    <%--                </div>--%>

                    <div class="form-row">
                        Bride Name: <form:input path="bride"
                                                cssClass="form-control"
                                                placeholder="Bride name.."
                                                aria-describedby="brideHelp"/>
                        <small id="brideHelp" class="form-text text-muted">
                            Type here Bride name.
                        </small>
                        <div><form:errors path="bride" cssClass="error"/></div>
                    </div>

                    <div class="form-row">
                        Bridegroom Name: <form:input path="bridegroom"
                                                     cssClass="form-control"
                                                     placeholder="Bridegroom name.."
                                                     aria-describedby="bridegroomHelp"/>
                        <small id="bridegroomHelp" class="form-text text-muted">
                            Type here bridegroom name.
                        </small>
                        <div><form:errors path="bridegroom" cssClass="error"/></div>
                    </div>

                    <div class="form-row">
                        Party Adress: <form:input path="address"
                                                  cssClass="form-control"
                                                  placeholder="address.."
                                                  aria-describedby="addressdHelp"/>
                        <small id="addressHelp" class="form-text text-muted">
                            Type here party address.
                        </small>
                        <div><form:errors path="address" cssClass="error"/></div>
                    </div>

                    <div class="form-row">
                        Music Type: <form:input path="music"
                                                cssClass="form-control"
                                                placeholder="music.."
                                                aria-describedby="musicHelp"/>
                        <small id="musicHelp" class="form-text text-muted">
                            Type here music type.
                        </small>
                        <div><form:errors path="music" cssClass="error"/></div>
                    </div>

                    <div class="form-row">
                        Date: <form:input type="date" path="date"
                                          cssClass="form-control"
                                          placeholder="date.."
                                          aria-describedby="dateHelp"/>
                        <small id="addressHelp" class="form-text text-muted">
                            Type here date.
                        </small>
                        <div><form:errors path="date" cssClass="error"/></div>
                    </div>


                    <div class="form-row">
                        <hr style="width: 50%; border: 1px dot-dash">
                    </div>


                    <div class="form-row">
                        <input type="submit" value="SAVE" class="btn btn-primary" style="width: 100%">
                    </div>
                    <div class="form-row">
                        <small onclick="goBack()" class="goBack"> <<< Go back?</small>
                    </div>


                </form:form>
            </div>
        </div>

    </div>

</div>
<%----------------------------------------------------------------------------%>
<%--FOOTER--%>
<%@include file="../parts/footer.jspf" %>
<%--FOOTER--%>
</body>
</html>
