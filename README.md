# Wedding-App

Final project for **CodersLab Java Developer Web** programming bootcamp


##Introduction

This application allows the user to create a showcase of wedding event. 
It's possible to register in app with one of two account types:
- As an Organizer: for creating event, update and remove it.
- As a Guest: only to view the event showcase.


##Preview

![wedding-app-screen-tour](wedding-app-screen-tour.gif)


##Technologies

1. Java
    - Spring Context
    - Spring MVC
    - Spring Data
    - Hibernate
    - Hibernate Validator
    - Model-View-Controller Pattern
    - Abstract classes
    - Interfaces
    - REGEX
    - Servlets
    
2. Databases

3. JSP
    - Taglib JSTL
    - Taglib form
    
4. JavaScript
    - Events
    
5. HTML i CSS
    - Bootstrap framework
    - Own styling


